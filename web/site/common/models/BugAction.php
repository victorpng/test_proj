<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "bug_action".
 *
 * @property int $id
 * @property int|null $bug_id
 * @property string|null $action_type
 * @property string|null $notes
 * @property string $delete_status
 * @property int|null $created_at
 * @property int|null $created_by
 */
class BugAction extends \common\components\MyCustomActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bug_action';
    }
    
    public function behaviors()
    {
        return [
            "timestamp" => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
            "blame" => [
                'class' => \yii\behaviors\BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by'],
                ],
            ],
            "auditTrail" => \common\behaviors\MyAuditTrailBehavior::className(),  
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bug_id', 'created_at', 'created_by'], 'integer'],
            [['action_type', 'notes', 'delete_status'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bug_id' => 'Bug ID',
            'action_type' => 'Action Type',
            'notes' => 'Notes',
            'delete_status' => 'Delete Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\BugActionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\BugActionQuery(get_called_class());
    }

    public static function makeModel($bug_id, $action_type){
        $m = new SELF();
        $m->bug_id = $bug_id;
        $m->action_type = $action_type;

        return $m;
    }
}
